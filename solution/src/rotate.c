#include "image.h"
#include <malloc.h>
#include <string.h>

#include "transform_status.h"


void transform_rotate_90_left(struct image *image, struct image* rotated_image) {
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            rotated_image->content[image->height - 1 - i + j * image->height] = image->content[j + i * image->width];

        }
    }
    rotated_image->width = image->height;
    rotated_image->height = image->width;
}

void transform_rotate_90_right(struct image *image, struct image* rotated_image) {
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            rotated_image->content[(image->width - 1 - j) * image->height + i] = image->content[j + i * image->width];
        }
    }
    rotated_image->width = image->height;
    rotated_image->height = image->width;
}

void transform_rotate_180(struct image *image, struct image* rotated_image) {
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            rotated_image->content[(image->width - 1 - j) + (image->height - 1 - i) * image->width] = image->content[j + i *
                                                                                                                image->width];
        }
    }
    rotated_image->width = image->width;
    rotated_image->height = image->height;
}

void transform_rotate_0(struct image *image, struct image* rotated_image) {
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            rotated_image->content[j + i *image->width] = image->content[j + i *image->width];
        }
    }
    rotated_image->width = image->width;
    rotated_image->height = image->height;
}

enum transform_status rotate(int angle, struct image *image, struct image* rotated_image) {
    while (angle < 0) angle += 360;
    *rotated_image = (struct image){0,0,NULL};
    if(angle % 90 != 0) return TRANSFORM_INVALID_ANGLE;
    struct pixel *rotated_image_content = malloc(image->height * image->width * sizeof(struct pixel));
    if(rotated_image_content == NULL) return TRANSFORM_MEM_FAILED;
    rotated_image->content = rotated_image_content;
    switch (angle) {
        case 0:
            transform_rotate_0(image, rotated_image);         //выглядит отвратительно, но компилятор ругался на memcpy
            break;
        case 90:
            transform_rotate_90_right(image, rotated_image);
            break;
        case 180:
            transform_rotate_180(image, rotated_image);
            break;
        case 270:
            transform_rotate_90_left(image, rotated_image);
            break;
        default:
            free(rotated_image_content);
            return TRANSFORM_INVALID_ANGLE;
    }
    return TRANSFORM_OK;
}
