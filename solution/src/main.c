#include "bmp.h"
#include "error_messages.h"
#include "image.h"
#include "rotate.h"
#include "transform_status.h"

// было бы круто сделать так, чтобы открывалось все через отдельную функцию, которая не зависит от формата, но такого в тз не было


int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // supress 'unused parameters' warning
    if (argc != 4) {
        printf("usage: in_path out_path angle");
        return 1;
    }
    const char *in_path = argv[1];
    const char *out_path = argv[2];
    int angle = atoi(argv[3]);
    struct image image = {0};

    FILE *in_file = fopen(in_path, "r");
    if (in_file == NULL) {
        perror(FILE_NOT_FOUND_ERROR_MESSAGE);
        return 1;
    }
    enum bmp_read read_result = read_bmp(in_file, &image);
    if (read_result != BMP_READ_OK) {
        perror(FILE_READ_ERROR_MESSAGE);
        perror(BMP_READ_STATUS_MESSAGES[read_result]);
        fclose(in_file);
        free(image.content);
        return 1;
    }
    fclose(in_file);

    struct image rotated_image;
    enum transform_status transform_status = rotate(angle, &image, &rotated_image);
    if(transform_status != TRANSFORM_OK){
        free(image.content);
        perror(TRANSFORM_FAILED_ERROR_MESSAGE);
        perror(TRANSFORM_STATUS_MESSAGES[transform_status]);
        return 1;
    }

    FILE *out_file = fopen(out_path, "w");
    if (out_file == NULL) {
        perror(COULDNT_OPEN_OUT_FILE_ERROR_MESSAGE);
        return 1;
    }
    enum bmp_write write_status = write_bmp(out_file, &rotated_image);

    fclose(out_file);
    free(image.content);
    free(rotated_image.content);
    if(write_status!=BMP_WRITE_OK){
        perror(FILE_WRITE_ERROR_MESSAGE);
        perror(BMP_WRITE_STATUS_MESSAGES[write_status]);
        return 1;
    }
    return 0;
}
