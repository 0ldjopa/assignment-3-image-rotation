#include "bmp.h"
#include <malloc.h>
#include <stddef.h>
#include <stdio.h>

#include "image.h"

struct __attribute__ ((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};



// достать картинку из памяти
enum bmp_read read_bmp(FILE* img, struct image *image) {
    struct bmp_header header = {0};

    if(!fread(&header, sizeof(header), 1, img)) return BMP_READ_ERROR;
    image->height = header.biHeight;
    image->width = header.biWidth;

    if(fseek(img, header.bOffBits, SEEK_SET)){
        return BMP_HEADER_ERROR;
    }

    size_t padding = 4 - (header.biWidth * sizeof(struct pixel)) % 4;
    image->content = malloc(header.biHeight * (header.biWidth * sizeof(struct pixel)+0));
    if(image->content == NULL){
//        free(image->content);
        return BMP_MEM_ERROR;
    }

    size_t pointer = 0;
    for (size_t j = 0; j < image->height-1; j++) {
        if(!fread(image->content+pointer, image->width * sizeof(struct pixel) + padding, 1,img)){
            free(image->content);
            return BMP_READ_ERROR;
        }
        pointer+= image->width;
    }
    if(!fread(image->content+pointer, image->width * sizeof(struct pixel), 1,img)){
        free(image->content);
        return BMP_READ_ERROR;
    }

    return BMP_READ_OK;
}

enum bmp_write write_bmp(FILE * out, struct image *image) {
    size_t padding = 4 - (image->width * sizeof(struct pixel)) % 4;
    size_t content_size = (image->height * (image->width * sizeof(struct pixel) + padding));
    int32_t header_size = sizeof(struct bmp_header);

    size_t offset = offsetof(struct bmp_header, biSize);
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = header_size + content_size,
            .bfReserved = 0,
            .bOffBits = header_size,
            .biSize = header_size - offset, // размер BITMAPFILEHEADER
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = sizeof(struct pixel) * 8,
            .biCompression = 0,
            .biSizeImage = content_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if(!fwrite(&header, header_size, 1, out)){
        return BMP_WRITE_FILE_ERR;
    }

    size_t pointer = 0;
    for (size_t j = 0; j < image->height; j++) {
        if(!fwrite(image->content+pointer, image->width * sizeof(struct pixel), 1,out)){
            return BMP_WRITE_FILE_ERR;
        }
        pointer+= image->width;
        if(fseek(out, (long)padding, SEEK_CUR)){
            return BMP_WRITE_FILE_ERR;
        }
    }
    return BMP_WRITE_OK;
}
