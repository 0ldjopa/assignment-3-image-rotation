#ifndef IMAGE_TRANSFORMER_ERROR_MESSAGES_H
#define IMAGE_TRANSFORMER_ERROR_MESSAGES_H

#define FILE_READ_ERROR_MESSAGE "Error reading file: "
#define FILE_NOT_FOUND_ERROR_MESSAGE "File not found"
#define TRANSFORM_FAILED_ERROR_MESSAGE "Transform failed"
#define COULDNT_OPEN_OUT_FILE_ERROR_MESSAGE "couldn't open output file"
#define FILE_WRITE_ERROR_MESSAGE "Error writing file: "


#endif
