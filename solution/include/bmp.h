#pragma once
#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

enum bmp_read{
    BMP_READ_OK = 0,
    BMP_HEADER_ERROR,
    BMP_READ_ERROR,
    BMP_MEM_ERROR,
    BMP_READ_COUNT
};

enum bmp_write{
    BMP_WRITE_OK = 0,
    BMP_WRITE_MEM_ERR,
    BMP_WRITE_FILE_ERR,
    BMP_WRITE_COUNT
};

static const char* const BMP_READ_STATUS_MESSAGES[BMP_READ_COUNT] = {
        [BMP_READ_OK] = "Ok",
        [BMP_READ_ERROR] = "Couldn't allocate memory",
        [BMP_MEM_ERROR] = "file read error",
        [BMP_HEADER_ERROR] = "Invalid header format",
};

static const char* const BMP_WRITE_STATUS_MESSAGES[BMP_WRITE_COUNT] = {
        [BMP_WRITE_OK] = "Ok",
        [BMP_WRITE_MEM_ERR] = "Couldn't allocate memory",
        [BMP_WRITE_FILE_ERR] = "Couldn't write to file",
};

enum bmp_read read_bmp(FILE* img, struct image *image);
enum bmp_write write_bmp(FILE * out, struct image *image);


#endif //IMAGE_TRANSFORMER_BMP_H
