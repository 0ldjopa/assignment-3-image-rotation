#pragma once

#include <inttypes.h>

struct __attribute__ ((packed)) pixel { uint8_t b, g, r; }; // почему интересно бгр?

struct image{
    uint64_t width, height;
    struct pixel* content; // Сохраняем в одномерный чтобы потому что не хочу с двухмерным возиться, да и тем более оно все будет известной размерности. Будем через ширину-высоту все менять
};
