#pragma once
#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"
#include "transform_status.h"

enum transform_status rotate(int angle, struct image* image, struct image* rotated_image);

#endif //IMAGE_TRANSFORMER_ROTATE_H
